Source: tensorpipe
Section: science
Homepage: https://github.com/pytorch/tensorpipe
Priority: optional
Standards-Version: 4.6.0
Vcs-Git: https://salsa.debian.org/deeplearning-team/tensorpipe.git
Vcs-Browser: https://salsa.debian.org/deeplearning-team/tensorpipe
Maintainer: Debian Deep Learning Team <debian-ai@lists.debian.org>
Uploaders: Mo Zhou <lumin@debian.org>
Build-Depends: cmake,
               debhelper-compat (= 13),
               libgtest-dev,
               libprotobuf-dev,
               libuv1-dev,
               pkg-config,
               protobuf-compiler,
               libnop-dev,
Rules-Requires-Root: no

Package: libtensorpipe-dev
Section: libdevel
Architecture: any
Depends: libtensorpipe0 (= ${binary:Version}), ${misc:Depends}, libnop-dev
Conflicts: libtensorpipe-cuda-dev
Replaces: libtensorpipe-cuda-dev
Description: tensor-aware point-to-point communication primitive for machine learning
 The TensorPipe project provides a tensor-aware channel to transfer rich
 objects from one process to another while using the fastest transport for the
 tensors contained therein (e.g., CUDA device-to-device copy).
 .
 This package contains development files.

Package: libtensorpipe0
Section: libs
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Multi-Arch: same
Conflicts: libtensorpipe-cuda-0
Replaces: libtensorpipe-cuda-0
Description: tensor-aware point-to-point communication primitive for machine learning
 The TensorPipe project provides a tensor-aware channel to transfer rich
 objects from one process to another while using the fastest transport for the
 tensors contained therein (e.g., CUDA device-to-device copy).
 .
 This package contains shared objects.
